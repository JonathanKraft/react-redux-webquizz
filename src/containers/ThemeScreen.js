import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setTheme } from "../actions/theme";
import { pushRoute } from "../actions/router";
import ThemeScreen from "../screens/ThemeScreen";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return { theme: state.theme.selectedTheme };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ setTheme, pushRoute }, dispatch)
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ThemeScreen);

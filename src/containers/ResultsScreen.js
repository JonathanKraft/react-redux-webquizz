import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ResultsScreen from "../screens/ResultsScreen";
import { pushRoute } from "../actions/router";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return { email: state.user.email, score: state.question.score, questions: state.question.questions };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ pushRoute }, dispatch)
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultsScreen);

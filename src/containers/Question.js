import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addAnswer } from "../actions/question";
import Question from "../components/Question";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return { points: state.question.points };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ addAnswer }, dispatch)
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Question);

import types from "../constants/actionTypes";

export const setTheme = theme => {
return { type: types.SET_THEME, theme };
};

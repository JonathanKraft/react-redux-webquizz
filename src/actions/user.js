import types from "../constants/actionTypes";

export const setUserEmail = email => {
return { type: types.SET_USER_EMAIL, email };
};

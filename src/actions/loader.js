
import types from "../constants/actionTypes";

export const showLoader = () => {
return { type: types.SHOW_LOADER };
};

export const hideLoader = () => {
return { type: types.HIDE_LOADER };
};

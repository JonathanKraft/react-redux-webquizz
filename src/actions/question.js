import types from "../constants/actionTypes";
import { baseURL } from "../config";
import { showLoader, hideLoader } from "./loader";

export const getQuestionsByLang = lang => {
  return async dispatch => {

    dispatch(showLoader());
    const response = await fetch(`${baseURL}${lang}.json`);
    const questions = await response.json();
    dispatch({
      type: types.GET_QUESTIONS,
      questions
    });
    dispatch(hideLoader());
  };
};

export const addAnswer = (indexCheckbox, value, indexQuestion) => {
  return { type: types.ADD_ANSWER, indexCheckbox, value, indexQuestion };
};

import React from "react";

const AppBar = (props) => {
  return (
    <nav>
      <div className="nav-wrapper">
        <span className="brand-logo center">Web Quizz</span>
      </div>
    </nav>
  )
}

export default AppBar;
import React from "react";

const Answer = (props) => {
  const { answer } = props;

  const onChange = (e) => {
    const { onChange, index } = props;
    onChange(index, e);
  }

    return (
      <div>
        <label htmlFor={answer.answer}>
          <input
            type="checkbox"
            onChange={onChange}
            id={answer.answer}
          />
          <span>{answer.answer}</span>
        </label>
      </div>
    );
}

export default Answer;
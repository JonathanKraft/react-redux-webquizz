import React from "react";
import Button from "../components/Button";

const GoBackButton = (props) => {
  const { actions } = props;
  return (
    <Button
      isGoBack
      label="Revenir en arrière"
      onClick={() => actions.getBack()}
    />
  );
}
export default GoBackButton;

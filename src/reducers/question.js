import types from "../constants/actionTypes";
import { countScore } from '../helpers/score';

const initialState = {
  questions: [],
  answers: {},
  score: 0,
};

const prepareAnswers = (answers, indexCheckbox, value, indexQuestion) => {
  const answerFromOneQuestion = answers[indexQuestion];
  return {
    ...answers,
    [indexQuestion]: {
      ...answerFromOneQuestion,
      [indexCheckbox]: value
    }
  }
}


export const question = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_QUESTIONS:
      return {
        ...state,
        questions: action.questions
      };
    case types.ADD_ANSWER:
      const newAnswers = prepareAnswers(state.answers, action.indexCheckbox, action.value, action.indexQuestion);
      return {
        ...state,
        answers: newAnswers,
        score: countScore(state.questions, newAnswers)
      };
    default:
      return state;
  }
};

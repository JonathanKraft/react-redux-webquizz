import React, { Component, Fragment } from "react";
import { renderCurrentRoute } from "./helpers/router";
import "./styles.css";
import AppBar from "./components/AppBar";
import "materialize-css/dist/css/materialize.min.css";

export default class App extends Component {
  render() {
    const { currentRouteName } = this.props;
    return (
      <Fragment>
        <AppBar />
        <main>{renderCurrentRoute(currentRouteName)}</main>
      </Fragment>
    );
  }
}


import React, { Component } from "react";
import Button from "../components/Button";


const ResultsScreen = (props) => {
  const { email, score, questions } = props;
  const handleEvent = event => {
    props.actions.pushRoute("theme");
  }
  
  return (
    <section className="result-screen">
      <p>{email}</p>
      <p>Score: {score} / {questions.length}</p>
      <Button label="Retour aux thèmes" onClick={handleEvent} />
    </section>
  );
}
export default ResultsScreen;

export const countScore = (questions, answers) => {
    return questions.reduce((acc, question, indexQuestion) => {
        return isAnswerGood(question, answers[indexQuestion]) ? acc + question.weight : acc
    }, 0)
};

const isAnswerGood = (question, answers) => {
    let correct =  question.answers.map((answer) => {
        return answer.correct;
    })
    let good = correct.reduce((acc, option, i) => {
        return (getFullAnswer(answers, i) !== option )? false : acc
    }, true)
    return good;
}

const getFullAnswer = (answers, i) => {
    if(typeof answers === "undefined") return false;
    else if(typeof answers[i] === "undefined") return false;
    else return answers[i];
}